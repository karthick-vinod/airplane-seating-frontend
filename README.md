# Airplane Problem UI

Hosted at https://airplane-problem-frontend.herokuapp.com/

### Useful commands:
1. Push and deploy to heroku alone: `git push heroku master`
2. Push to Bitbucket alone: `git push origin master`
3. Push to both heroku and bitbucket: `git push all master`
4. Tail heroku logs: `heroku logs --tail -a airplane-problem-frontend `
