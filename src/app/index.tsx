// Currently, @babel/preset-env is unaware that using import() with Webpack
// relies on Promise internally. Environments which do not have builtin support
// for Promise, like Internet Explorer, will require both the promise and
// iterator polyfills be added manually.
//
// eslint-disable-next-line max-len
// https://babeljs.io/docs/en/next/babel-plugin-syntax-dynamic-import#working-with-webpack-and-babel-preset-env
// https://github.com/babel/babel/issues/9872
// https://github.com/webpack/webpack/issues/8656

// eslint-disable-next-line import/no-extraneous-dependencies
import 'core-js/modules/es.array.iterator';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'core-js/modules/es.promise';

import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { ConnectedRouter } from 'connected-react-router';

import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';

import App from './component/App';
import { configureStore, history } from './store';
import { loadSegment } from './util/analytics';

loadSegment();

const app = (
    <Provider store={configureStore({})}>
        <ThemeProvider theme={{}}>
            <CssBaseline />
            <ConnectedRouter history={history}>
                <App />
            </ConnectedRouter>
        </ThemeProvider>
    </Provider>
);

render(app, document.querySelector('#app'));
