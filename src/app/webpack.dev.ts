/* eslint-disable import/first */
import { resolve } from 'path';

process.env.NODE_CONFIG_DIR = resolve(__dirname, 'config');
process.env.NODE_CONFIG_STRICT_MODE = 'true';

import * as config from 'config';

import ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
import HtmlWebpackPlugin = require('html-webpack-plugin');
// import HtmlWebpackPlugin from 'html-webpack-plugin';
import { Configuration, DefinePlugin, NamedModulesPlugin } from 'webpack';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const publicPath = '/';

const webpackConfig: Configuration = {
    name: 'app',
    mode: 'development',
    context: __dirname,
    devtool: 'eval-source-map',
    entry: './index.tsx',

    output: {
        publicPath,
        filename: '[name].js',
    },

    module: {
        rules: [
            {
                test: /\.(j|t)sx?$/,
                exclude: /node_modules/,
                include: [resolve(__dirname), resolve(__dirname, '../shared')],
                use: {
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true,
                        babelrc: false,
                        presets: [
                            [
                                '@babel/preset-env',
                                {
                                    targets: { browsers: 'last 2 versions' },
                                    useBuiltIns: 'usage',
                                    corejs: 3,
                                },
                            ],
                            '@babel/preset-typescript',
                            '@babel/preset-react',
                        ],
                        plugins: [
                            '@babel/proposal-class-properties',
                            '@babel/plugin-syntax-dynamic-import',
                        ],
                    },
                },
            },
        ],
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
        alias: {
            'react-dom': '@hot-loader/react-dom',
        },
    },

    plugins: [
        new DefinePlugin({ CONFIG: JSON.stringify(config) }),
        new NamedModulesPlugin(),
        new HtmlWebpackPlugin({ template: './public/index.html' }),
        new FaviconsWebpackPlugin({
            logo: './public/favicon.png',
        }),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
            eslint: true,
        }),
    ],
};

export default webpackConfig;
