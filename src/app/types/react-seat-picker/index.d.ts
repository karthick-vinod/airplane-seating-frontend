/* eslint-disable vars-on-top */
/* eslint-disable no-var */

interface SeatPickerProps {
    rows: any;
    alpha: boolean;
    visible: boolean;
    selectedByDefault: boolean;
    loading?: boolean;
    [k: string]: any;
}

declare const SeatPicker: SeatPicker;

declare module 'react-seat-picker' {
    export default SeatPicker;
}
