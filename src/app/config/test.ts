import { Config, Stage } from './types';

const testConfig: Partial<Config> = {
    stage: Stage.Test,
    baseURL: 'testApiUrl',
};

export default testConfig;
