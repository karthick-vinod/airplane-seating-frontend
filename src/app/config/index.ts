import { Config, Stage as StageEnum } from './types';

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
export default CONFIG as Config;
export const Stage = StageEnum;
