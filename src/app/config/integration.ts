import stagingConfig from './staging';
import { Config } from './types';

const integrationConfig: Partial<Config> = {
    ...stagingConfig,
    segmentKey: '',
};

export default integrationConfig;
