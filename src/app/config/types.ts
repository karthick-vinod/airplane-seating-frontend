type Paths = { [key: string]: string | undefined } & {
    airplanes: string;
    airplane: string;
};

export enum Stage {
    Development = 'development',
    Test = 'test',
    Dev = 'dev',
    QA = 'qa',
    Staging = 'staging',
    Production = 'production',
    Unknown = 'unknown',
}

export interface Config {
    baseURL: string;
    stage: Stage;
    paths: Paths;
    segmentKey: string;
    newRelicApplicationId?: string;
}
