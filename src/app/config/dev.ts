import { Config, Stage } from './types';

const devConfig: Partial<Config> = {
    stage: Stage.Dev,
};

export default devConfig;
