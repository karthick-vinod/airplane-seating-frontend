import { Config, Stage } from './types';

const stagingConfig: Partial<Config> = {
    stage: Stage.Staging,
};

export default stagingConfig;
