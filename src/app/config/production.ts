import { Config, Stage } from './types';

const productionConfig: Partial<Config> = {
    stage: Stage.Production,
};

export default productionConfig;
