import { Config, Stage } from './types';

const developmentConfig: Partial<Config> = {
    stage: Stage.Development,
    baseURL: 'http://localhost:4000/api',
};

export default developmentConfig;
