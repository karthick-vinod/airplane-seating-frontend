import { Config, Stage } from './types';

const defaultConfig: Config = {
    stage: Stage.Unknown,
    baseURL: '/api',
    paths: {
        airplanes: '/airplanes/:id',
        airplane: '/airplanes',
    },
    segmentKey: '',
    newRelicApplicationId: undefined,
};

export default defaultConfig;
