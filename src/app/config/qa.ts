import { Config, Stage } from './types';

const qaConfig: Partial<Config> = {
    stage: Stage.QA,
};

export default qaConfig;
