import { StaticContext } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

import { ApplicationState } from '../../store';

export function mockLocation(partial?: Partial<Location>) {
    return {
        hash: '',
        key: '',
        pathname: '',
        search: '',
        state: {},
        ...partial,
    };
}

export function mockRouteComponentProps(
    partial?: Partial<RouteComponentProps<any, StaticContext, any>>
): RouteComponentProps<any, StaticContext, any> {
    return {
        location: { hash: '', key: '', pathname: '', search: '', state: {} },
        match: { isExact: true, params: {}, path: '', url: '' },
        history: {
            location: mockLocation(),
            length: 2,
            action: 'POP',
            push: jest.fn(),
            replace: jest.fn(),
            go: jest.fn(),
            goBack: jest.fn(),
            goForward: jest.fn(),
            block: jest.fn(),
            createHref: jest.fn(),
            listen: jest.fn(),
        },
        staticContext: {},
        ...partial,
    };
}

export function mockApplicationState(
    partial?: Partial<ApplicationState>
): ApplicationState {
    return {
        router: { location: mockLocation(), action: 'POP' },
        ...partial,
    };
}
