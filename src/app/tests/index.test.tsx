import { render } from 'react-dom';
import { configureStore } from '../store';

// eslint-disable-next-line no-null/no-null
document.querySelector = jest.fn(() => null);

jest.mock('react-dom', () => ({ render: jest.fn() }));
jest.mock('../store', () => ({
    configureStore: jest.fn(() => ({
        dispatch: jest.fn(),
        subscribe: jest.fn(),
        getState: jest.fn(),
    })),
}));

test('should configure store and render', () => {
    // eslint-disable-next-line global-require
    require('..');

    expect(configureStore).toBeCalledWith({});
    expect(document.querySelector).toBeCalledWith('#app');

    // eslint-disable-next-line no-null/no-null
    expect(render).toBeCalledWith(expect.any(Object), null);
});
