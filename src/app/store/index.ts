import { Store, applyMiddleware, compose, createStore } from 'redux';
import reduxThunk from 'redux-thunk';

import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

// import config, { Stage } from '../config';
import analyticsTracker from './analyticsTracker';
import reducers, { ApplicationState as State } from './reducers';

// The top-level state object
export type ApplicationState = State;

// https://github.com/supasate/connected-react-router#usage
export const history = createBrowserHistory();

// if (config.stage !== Stage.Production) middleware.push(reduxLogger);

export function configureStore(
    initialState: ApplicationState | {}
): Store<ApplicationState> {
    const composeEnhancer: typeof compose =
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    // We'll create our store with the combined reducers/sagas, and the initial
    // Redux state that we'll be passing from our entry point.
    const store = createStore(
        reducers(history),
        initialState,
        composeEnhancer(
            applyMiddleware(
                ...[
                    reduxThunk,
                    // ...middleware,
                    analyticsTracker,
                    routerMiddleware(history),
                ]
            )
        )
    );

    /* istanbul ignore if  */
    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('./reducers', () => {
            store.replaceReducer(reducers(history));
        });
    }

    return store;
}
