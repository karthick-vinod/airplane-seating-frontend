import { combineReducers } from 'redux';

import { RouterState, connectRouter } from 'connected-react-router';
import { History } from 'history';

// The top-level state object
export interface ApplicationState {
    router: RouterState;
}

// Whenever an action is dispatched, Redux will update each top-level
// application state property using the reducer with the matching name. It's
// important that the names match exactly, and that the reducer acts on the
// corresponding ApplicationState property type.
export default (history: History) =>
    combineReducers<ApplicationState>({
        router: connectRouter(history),
    });
