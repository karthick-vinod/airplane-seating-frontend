import { LOCATION_CHANGE, RouterState } from 'connected-react-router';
import { createTracker } from 'redux-segment';

import { segmentActionCreators } from '../util/analytics';
import pageKeyMatch from '../util/pageKeyMatch';
import { ApplicationState } from './reducers';

const { page } = segmentActionCreators;

interface PageData {
    name?: string;
    properties?: any;
}

const pageData: { [k: string]: (p: { [key: string]: string }) => PageData } = {
    cards: () => ({ name: 'Cards' }),
    dice: () => ({ name: 'Dice' }),
};

function pageMatch(path: string): PageData {
    const { params, pageKey } = pageKeyMatch(path);
    if (!pageKey || !pageData[pageKey]) return {};
    return pageData[pageKey](params);
}

/**
 * https://github.com/rangle/redux-segment#installation
 * Inserted into redux-segment's tracker for sending events to Segment.
 */

export const mapper = {
    [LOCATION_CHANGE]: (
        getState: () => ApplicationState,
        { payload }: { payload: RouterState }
    ) => {
        const { name, properties } = pageMatch(payload.location.pathname);

        return page(name, {
            ...properties,
            url: window.location.href,
            path: payload.location.pathname,
            referrer: getState().router.location.pathname,
            // We are explicitly not tracking the title since it is updated
            // asynchronously and will never be correct at this point.
            title: '',
            referrerTitle: document.title,
        });
    },
};

export default createTracker({ mapper });
