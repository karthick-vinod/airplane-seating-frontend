import * as React from 'react';
import {
    TableContainer,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    Zoom,
    Fab,
    withStyles,
    Popover,
    TextField,
    Grid,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import axios from 'axios';
import { RouteComponentProps, StaticContext, withRouter } from 'react-router';
import AeroplaneDetails from '../Aeroplane';

export interface Aeroplane {
    id: string;
    seatStructure: number[][];
    seatRows: SeatRow[];
}

export interface SeatRow {
    seatBlocks: SeatBlock[];
}

export interface SeatBlock {
    seats: Seat[];
    blockType: 'LEFT_CORNER' | 'MIDDLE' | 'RIGHT_CORNER';
    vacancy: { totalSeats: number };
}

export interface Seat {
    seatType: 'WINDOW' | 'MIDDLE' | 'AISLE';
    row: number;
    column: number;
    passenger: number;
    vacant: false;
}

interface State {
    aeroplanes: Aeroplane[];
    openDialog: boolean;
    aeroplane?: Aeroplane;
    anchorEl?: EventTarget;
    title: string;
    structure: string;
}

export type DashboardProps = RouteComponentProps<any, StaticContext, any>;

const API = {
    getAeroplanes: () =>
        axios.get<Aeroplane[]>(
            `https://airplane-problem-backend.herokuapp.com/aeroplanes/`
        ),

    getAeroplane: (id: string) =>
        axios.get<Aeroplane>(
            `https://airplane-problem-backend.herokuapp.com/aeroplanes/${id}`
        ),

    createAeroplane: (id: string, structure: string) =>
        axios.post<Aeroplane>(
            `https://airplane-problem-backend.herokuapp.com/aeroplanes/${id}`,
            JSON.parse(structure)
        ),

    allocateseat: (id: string) =>
        axios.post<Aeroplane>(
            `https://airplane-problem-backend.herokuapp.com/aeroplanes/${id}/allocate_seat`
        ),

    resetAeroplane: (id: string) =>
        axios.post<Aeroplane>(
            `https://airplane-problem-backend.herokuapp.com/aeroplanes/${id}/reset`
        ),
};

const Fabi = withStyles((theme) => ({
    root: {
        position: 'absolute',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
}))(Fab);

// eslint-disable-next-line react/prefer-stateless-function
class Dashboard extends React.Component<DashboardProps, State> {
    constructor(props: DashboardProps) {
        super(props);
        this.state = {
            aeroplanes: [] as Aeroplane[],
            openDialog: false,
            title: '',
            structure: '',
        };
    }

    async componentDidMount() {
        const aeroplanes = (await API.getAeroplanes()).data;
        this.setState({ aeroplanes });
    }

    showDetails = (aeroplane: Aeroplane) => () =>
        this.setState({ aeroplane, openDialog: true });

    // eslint-disable-next-line class-methods-use-this
    allocateSeat = async (id: string) => {
        const response = await API.allocateseat(id);
        this.setState({ aeroplane: response.data });
    };

    resetAeroplane = async (id: string) => {
        await API.resetAeroplane(id);
        const response = await API.getAeroplane(id);
        this.setState({ aeroplane: response.data });
    };

    closeDialog = () =>
        this.setState({ openDialog: false, aeroplane: undefined });

    anchorToFab = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) =>
        this.setState({ anchorEl: event.currentTarget });

    handlePopupClose = () => this.setState({ anchorEl: undefined });

    updateTitle: React.FocusEventHandler<HTMLInputElement> = (event) =>
        this.setState({ title: event.target.value });

    updateStructure: React.FocusEventHandler<HTMLInputElement> = (event) =>
        this.setState({ structure: event.target.value });

    createPlane = async () => {
        API.createAeroplane(this.state.title, this.state.structure);
        await new Promise((r) => setTimeout(r, 500));
        const response = await API.getAeroplanes();
        this.setState({
            aeroplanes: response.data,
            title: '',
            structure: '',
            anchorEl: undefined,
        });
    };

    render() {
        return (
            <>
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Aeroplane Id</TableCell>
                                <TableCell />
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.aeroplanes.map((plane) => (
                                <TableRow key={plane.id}>
                                    <TableCell component="th" scope="row">
                                        {plane.id}
                                    </TableCell>
                                    <TableCell align="right">
                                        <Button
                                            onClick={this.showDetails(plane)}
                                        >
                                            VIEW
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Dialog
                    open={this.state.openDialog && !!this.state.aeroplane}
                    onClose={this.closeDialog}
                    fullWidth
                    maxWidth="xl"
                >
                    <DialogTitle>PLANE: {this.state.aeroplane?.id}</DialogTitle>
                    <DialogContent>
                        {this.state.aeroplane && (
                            <AeroplaneDetails
                                aeroplane={this.state.aeroplane}
                                allocateSeat={this.allocateSeat}
                                resetAeroplane={this.resetAeroplane}
                            />
                        )}
                    </DialogContent>
                </Dialog>
                <Zoom
                    key="primary"
                    in
                    timeout={500}
                    style={{
                        transitionDelay: `500ms`,
                    }}
                    unmountOnExit
                >
                    <Fabi
                        aria-label="Add"
                        color={'primary' as 'primary'}
                        onClick={this.anchorToFab}
                    >
                        <AddIcon />
                    </Fabi>
                </Zoom>
                <Popover
                    open={!!this.state.anchorEl}
                    anchorEl={this.state.anchorEl as Element}
                    onClose={this.handlePopupClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <Grid
                        style={{ padding: '10px' }}
                        container
                        alignItems="center"
                        justify="center"
                        spacing={2}
                    >
                        <Grid item xs={12}>
                            <TextField
                                style={{ width: '100%' }}
                                variant="filled"
                                placeholder="Airplane Title"
                                onBlur={this.updateTitle}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                style={{ width: '100%' }}
                                variant="filled"
                                placeholder="Seat structure as valid JSON"
                                onBlur={this.updateStructure}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={this.createPlane}
                            >
                                CREATE
                            </Button>
                        </Grid>
                    </Grid>
                </Popover>
            </>
        );
    }
}

export default withRouter(Dashboard);
