import * as React from 'react';
import { hot } from 'react-hot-loader';

import classNames from 'classnames';

import { StandardProps, Theme } from '@material-ui/core';
import { createStyles, withStyles } from '@material-ui/core/styles';
import { ClassNameMap } from '@material-ui/styles/withStyles';

import Content from './Content';
import Footer from './Footer';
import Header from './Header';

type ClassKey = 'root' | 'header' | 'content' | 'footer' | 'constrainedWidth';

interface Props
    extends StandardProps<React.HTMLAttributes<HTMLDivElement>, ClassKey> {
    classes: ClassNameMap<ClassKey>;
}

const styles = ({ palette }: Theme) => {
    const footerHeight = '5.5rem';

    const safeFooterPadding = 'env(safe-area-inset-bottom)';
    const safeFooterH = `calc(${footerHeight} + ${safeFooterPadding})`;

    return createStyles({
        '@global': {
            'html, body, div#app': {
                height: '100%',
                backgroundColor: palette.background.default,
            },
        },
        root: {
            minHeight: '100%',
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
        },
        header: { alignItems: 'center' },
        content: {
            flexGrow: 1,
            justifyContent: 'center',
            '@supports not (min-height: calc(100% - 1em))': {
                paddingBottom: footerHeight,
            },
        },
        footer: {
            minHeight: footerHeight,
            '@supports (min-height: calc(100% - 1em))': {
                minHeight: safeFooterH,
                paddingBottom: safeFooterPadding,
            },
        },
        constrainedWidth: {
            width: '100%',
        },
    });
};

const App = React.forwardRef<HTMLDivElement, Props>(
    ({ classes, className, ...other }: Props, ref) => (
        <div
            className={classNames(classes.root, className)}
            {...other}
            ref={ref}
        >
            <Header
                className={classes.header}
                classes={{
                    toolbar: classes.constrainedWidth,
                    tabs: classes.constrainedWidth,
                }}
            />
            <Content
                className={classes.content}
                classes={{ content: classes.constrainedWidth }}
            />
            <Footer />
        </div>
    )
);

App.displayName = 'App';

export default hot(module)(withStyles(styles)(App));
