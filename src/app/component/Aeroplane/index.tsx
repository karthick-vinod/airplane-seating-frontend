/* eslint-disable unicorn/no-nested-ternary */
import * as React from 'react';
import SeatPicker from 'react-seat-picker';
import { Aeroplane, SeatBlock, Seat } from 'component/Dashboard';
import { Grid, Button } from '@material-ui/core';

interface Props {
    aeroplane: Aeroplane;
    allocateSeat: (id: string) => Promise<void>;
    resetAeroplane: (id: string) => Promise<void>;
}

interface Row {
    id: number;
    number: number;
    isSelected: boolean;
    isReserved: boolean;
    orientation?: 'east' | 'west';
    tooltip?: string;
}

type Rows = (Row | null)[];

export default class Airplane extends React.Component<Props> {
    onAllocate = async () => {
        await this.props.allocateSeat(this.props.aeroplane.id);
    };

    onResetPlane = async () => {
        await this.props.resetAeroplane(this.props.aeroplane.id);
    };

    getRowForSeat = (id: number, block: SeatBlock, seat: Seat): Row => {
        const row: Partial<Row> = {
            id,
            number: id,
            isReserved: seat.passenger === null,
            orientation:
                block.blockType === 'LEFT_CORNER'
                    ? 'west'
                    : block.blockType === 'RIGHT_CORNER'
                    ? 'east'
                    : undefined,
            tooltip: seat.passenger
                ? `Reserved for passenger ${seat.passenger}`
                : undefined,
        };
        return row as Row;
    };

    getRows = (): Rows[] => {
        const rows: Rows[] = [];
        const { seatRows, seatStructure } = this.props.aeroplane;

        let seatCounter = 0;
        seatRows.forEach((seatRow) => {
            const row: Rows = [];
            seatRow.seatBlocks.forEach((seatBlock, blockIndex) => {
                if (seatBlock.seats.length === 0) {
                    for (
                        let index = 0;
                        index < seatStructure[blockIndex][0];
                        index += 1
                    ) {
                        row.push(null);
                    }
                }
                seatBlock.seats.forEach((seat) => {
                    seatCounter += 1;
                    row.push(this.getRowForSeat(seatCounter, seatBlock, seat));
                });
                row.push(null);
            });
            rows.push(row);
        });

        return rows;
    };

    render() {
        return (
            <Grid
                container
                style={{ padding: '10px' }}
                alignItems="center"
                justify="center"
                spacing={2}
            >
                <Grid item xs={10}>
                    <SeatPicker
                        key={Math.random()}
                        rows={this.getRows()}
                        alpha={false}
                        visible
                        selectedByDefault
                        tooltipProps={{ multiline: true }}
                    />
                </Grid>
                <Grid
                    item
                    container
                    xs={2}
                    alignItems="center"
                    justify="center"
                    spacing={2}
                >
                    <Grid item xs={12}>
                        <Button
                            onClick={this.onAllocate}
                            variant="contained"
                            color="primary"
                        >
                            Allocate Seat
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            onClick={this.onResetPlane}
                            variant="contained"
                            color="primary"
                        >
                            Reset Plane
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}
