import * as React from 'react';
import { StaticContext } from 'react-router';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import classNames from 'classnames';

import AppBar, { AppBarClassKey, AppBarProps } from '@material-ui/core/AppBar';
import { createStyles, withStyles } from '@material-ui/core/styles';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import Toolbar from '@material-ui/core/Toolbar';

import { AirplaneIcon } from '../icons/Airplane';

type ClassKey = 'toolbar' | 'tabs' | AppBarClassKey;

interface Props
    extends AppBarProps,
        RouteComponentProps<any, StaticContext, any> {
    classes: ClassNameMap<ClassKey>;
}

const styles = createStyles({
    root: {},
    toolbar: {},
    tabs: {},
    positionFixed: {},
    positionAbsolute: {},
    positionSticky: {},
    positionStatic: {},
    positionRelative: {},
    colorDefault: {},
    colorPrimary: {},
    colorSecondary: {},
});

const Header = ({ classes, className }: Props) => (
    <AppBar
        position="static"
        color="primary"
        className={classNames(classes.root, className)}
    >
        <Toolbar className={classes.toolbar}>
            <AirplaneIcon />
        </Toolbar>
    </AppBar>
);

export default withStyles(styles)(withRouter(Header));
