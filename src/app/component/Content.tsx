import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

import classNames from 'classnames';

import { StandardProps, Theme } from '@material-ui/core';
import { createStyles, withStyles } from '@material-ui/core/styles';
import { ClassNameMap } from '@material-ui/styles/withStyles';

import Dashboard from './Dashboard';

type ClassKey = 'root' | 'content';

interface Props
    extends StandardProps<React.HTMLAttributes<HTMLDivElement>, ClassKey> {
    classes: ClassNameMap<ClassKey>;
}

const styles = ({ palette }: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            backgroundColor: palette.background.default,
        },
        content: { padding: '1rem' },
    });

const Content = React.forwardRef<HTMLDivElement, Props>(
    ({ classes, className, ...props }: Props, ref) => (
        <div
            className={classNames(classes.root, className)}
            {...props}
            ref={ref}
        >
            <div className={classes.content}>
                <React.Suspense fallback={<div />}>
                    <Switch>
                        <Route<any> exact path="/" render={Dashboard} />
                    </Switch>
                </React.Suspense>
            </div>
        </div>
    )
);

Content.displayName = 'Content';

export default withStyles(styles)(Content);
