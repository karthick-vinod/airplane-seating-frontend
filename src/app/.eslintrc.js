module.exports = {
    extends: ['../../eslint/base', '../../eslint/jest'],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: 'src/app/tsconfig.json',
        sourceType: 'module',
    },
    settings: {
        'import/parsers': {
            '@typescript-eslint/parser': ['.ts', '.tsx'],
        },
        'import/resolver': {
            // use <root>/path/to/folder/tsconfig.json
            typescript: {
                directory: 'src/app',
            },
        },
    },
};
