import { match, matchPath } from 'react-router';

import config from '../config';

const { paths } = config;

export default (path: string): match & { pageKey: string | undefined } => {
    const flippedPaths: { [path: string]: string } = Object.keys(paths).reduce(
        (acc, key) => ({ ...acc, [paths[key] as string]: key }),
        {}
    );

    const pagePaths = Object.keys(flippedPaths);
    const defaultMatchRes = { params: {}, isExact: false, path: '', url: path };

    const matchRes =
        matchPath(path, { path: pagePaths, exact: true }) || defaultMatchRes;

    const pageKey = flippedPaths[matchRes.path];
    return { ...matchRes, pageKey };
};
