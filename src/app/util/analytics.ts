import { EventTypes } from 'redux-segment';

import config from '../config';

declare global {
    interface Window {
        analytics: SegmentAnalytics.AnalyticsJS;
    }
}

export function loadSegment() {
    if (!config.segmentKey) return;
    window.analytics.load(config.segmentKey);
}

export const segmentActionCreators = {
    /**
     * Used to track events.
     *
     * https://segment.com/docs/spec/track/
     *
     * @param event       The name of the event being tracked.
     * @param properties  Any properties worth recording with the event.
     */

    track: (event: string, properties?: any) => ({
        eventType: EventTypes.track,
        eventPayload: { event, properties },
    }),

    /**
     * Used to identify a user.
     *
     * https://segment.com/docs/spec/identify/
     *
     * @param userId  Any unique id that can be associated with a user.
     * @param traits  User traits. (ex. email, firstName, lastName).
     */

    identify: (userId?: string, traits?: any) => ({
        eventType: EventTypes.identify,
        eventPayload: { userId, traits },
    }),

    /**
     * Mixpanel needs alias to be called exactly one time before the first
     * identify in order to create a person profile. The Segment documentation
     * is misleading. Alias can be called without a previous id. In customer-ui
     * alias is called when a user first sets their password.
     *
     * https://segment.com/docs/spec/alias/
     * https://segment.com/docs/destinations/mixpanel/
     *
     * @param userId Any unique id that can be associated with a user.
     */

    alias: (userId?: string) => ({
        eventType: EventTypes.alias,
        eventPayload: { userId },
    }),

    /**
     * For page load events.
     *
     * @param name        Name of the page.
     * @param properties  Any interesting page properties.
     */
    page: (name?: string, properties?: any) => ({
        eventType: EventTypes.page,
        eventPayload: { name, properties },
    }),
};
