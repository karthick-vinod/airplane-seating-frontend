import { EventTypes } from 'redux-segment';

import config from '../../config';
import { loadSegment, segmentActionCreators } from '../analytics';

const { alias, identify, page, track } = segmentActionCreators;

describe('loadSegment', () => {
    let analyticsBak: SegmentAnalytics.AnalyticsJS;

    beforeAll(() => {
        analyticsBak = window.analytics;
        window.analytics = (jest.fn() as unknown) as SegmentAnalytics.AnalyticsJS;
    });

    afterAll(() => {
        window.analytics = analyticsBak;
    });

    test('should call window.analytics.load when segmentKey is set', () => {
        window.analytics.load = jest.fn();
        config.segmentKey = 'hello there';
        loadSegment();
        expect(window.analytics.load).toBeCalled();
    });

    test('should not call window.analytics.load when no segmentKey set', () => {
        window.analytics.load = jest.fn();
        config.segmentKey = '';
        loadSegment();
        expect(window.analytics.load).not.toBeCalled();
    });
});

describe('segmentActionCreators', () => {
    describe('track', () => {
        const event = 'Test Event';

        test('should return track event metadata without properties', () => {
            const expected = {
                eventType: EventTypes.track,
                eventPayload: { event },
            };

            const actual = track(event);
            expect(actual).toMatchObject(expected);
        });

        test('should return track event metadata with properties', () => {
            const properties = {
                email: 'test@email.com',
                firstName: 'First',
                lastName: 'Last',
            };

            const expected = {
                eventType: EventTypes.track,
                eventPayload: { event, properties },
            };

            const actual = track(event, properties);
            expect(actual).toMatchObject(expected);
        });
    });

    describe('identify', () => {
        const userId = '123';

        test('should return identify event metadata without traits', () => {
            const expected = {
                eventType: EventTypes.identify,
                eventPayload: { userId },
            };

            const actual = identify(userId);
            expect(actual).toMatchObject(expected);
        });

        test('should return identify event metadata with traits', () => {
            const traits = {
                email: 'test@email.com',
                firstName: 'First',
                lastName: 'Last',
            };

            const expected = {
                eventType: EventTypes.identify,
                eventPayload: { userId, traits },
            };

            const actual = identify(userId, traits);
            expect(actual).toMatchObject(expected);
        });
    });

    describe('alias', () => {
        test('should return alias event metadata', () => {
            const userId = '123';

            const expected = {
                eventType: EventTypes.alias,
                eventPayload: { userId },
            };

            const actual = alias(userId);
            expect(actual).toMatchObject(expected);
        });
    });

    describe('page', () => {
        test('should return page event metadata', () => {
            const name = '/sign-in';
            const properties = { referrer: '/', url: 'http://test.com/' };

            const expected = {
                eventType: EventTypes.page,
                eventPayload: { name, properties },
            };

            const actual = page(name, properties);
            expect(actual).toMatchObject(expected);
        });
    });
});
