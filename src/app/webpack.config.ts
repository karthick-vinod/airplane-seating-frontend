import { join, resolve } from 'path';
import { Configuration, DefinePlugin, HashedModuleIdsPlugin } from 'webpack';

import HtmlWebpackPlugin = require('html-webpack-plugin');
import CompressWebpackPlugin = require('compression-webpack-plugin');

/* eslint-disable @typescript-eslint/no-var-requires */
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const importFresh = require('import-fresh');
/* eslint-enable @typescript-eslint/no-var-requires */

process.env.NODE_CONFIG_DIR = resolve(__dirname, 'config');
process.env.NODE_CONFIG_STRICT_MODE = 'true';

const config = importFresh('config');
const publicPath = '/';

const webpackConfig: Configuration = {
    context: __dirname,
    name: 'app',
    mode: 'production',
    devtool: 'eval-source-map',
    entry: './index.tsx',

    output: {
        publicPath,
        path: join(__dirname, '..', '..', 'dist', 'assets'),
        filename: '[name].[contenthash].js',
    },

    module: {
        rules: [
            {
                test: /\.(j|t)sx?$/,
                include: [resolve(__dirname), resolve(__dirname, '../shared')],
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true,
                        babelrc: false,
                        presets: [
                            [
                                '@babel/preset-env',
                                {
                                    targets: { browsers: 'last 2 versions' },
                                    useBuiltIns: 'usage',
                                    corejs: 3,
                                },
                            ],
                            '@babel/preset-typescript',
                            '@babel/preset-react',
                        ],
                        plugins: [
                            '@babel/proposal-class-properties',
                            '@babel/plugin-syntax-dynamic-import',
                            [
                                'babel-plugin-jsx-remove-data-test-id',
                                { attributes: 'data-testid' },
                            ],
                        ],
                    },
                },
            },
        ],
    },

    optimization: {
        minimize: true,
        usedExports: true,
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[/\\]node_modules[/\\]/,
                    name: 'vendor',
                    chunks: 'all',
                },
            },
        },
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
    },

    plugins: [
        new DefinePlugin({ CONFIG: JSON.stringify(config) }),
        new CompressWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './public/index.html',
        }),
        new HashedModuleIdsPlugin(),
        new FaviconsWebpackPlugin({
            logo: './public/favicon.png',
        }),
    ],
};

export default webpackConfig;
