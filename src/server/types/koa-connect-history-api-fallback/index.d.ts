/* eslint-disable no-redeclare */
// eslint-disable-next-line import/no-extraneous-dependencies
import * as Koa from 'koa';

declare function koaConnectHistoryApiFallback(
    options?: koaConnectHistoryApiFallback.Options
): Koa.Middleware;

declare namespace koaConnectHistoryApiFallback {
    interface Options {
        htmlAcceptHeaders?: string[];
    }
}

export = koaConnectHistoryApiFallback;
