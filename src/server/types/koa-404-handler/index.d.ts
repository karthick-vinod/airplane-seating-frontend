/* eslint-disable no-redeclare */
// eslint-disable-next-line import/no-extraneous-dependencies
import * as Koa from 'koa';

declare function koa404Handler(): Koa.Middleware;
declare namespace koa404Handler {}
export = koa404Handler;
