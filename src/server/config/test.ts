import { Config, Stage } from './types';

const developmentConfig: Partial<Config> = {
    stage: Stage.Test,
    useConsoleLogger: false,
    logSeverity: 'debug',
};

export default developmentConfig;
