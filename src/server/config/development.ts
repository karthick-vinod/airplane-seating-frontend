import { Config, Stage } from './types';

const developmentConfig: Partial<Config> = {
    stage: Stage.Development,
    useConsoleLogger: true,
    logSeverity: 'debug',
};

export default developmentConfig;
