import { Config, Stage } from './types';

const productionConfig: Partial<Config> = {
    stage: Stage.Production,
    instrumentForNewrelic: true,
};

export default productionConfig;
