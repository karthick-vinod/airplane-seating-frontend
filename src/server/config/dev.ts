import { Config, Stage } from './types';

const devConfig: Partial<Config> = {
    stage: Stage.Dev,
    instrumentForNewrelic: true,
};

export default devConfig;
