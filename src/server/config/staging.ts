import { Config, Stage } from './types';

const stagingConfig: Partial<Config> = {
    stage: Stage.Staging,
    instrumentForNewrelic: true,
};

export default stagingConfig;
