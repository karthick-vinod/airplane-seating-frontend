import { Config, Stage } from './types';

const defaultConfig: Config = {
    stage: Stage.Unknown,
    useConsoleLogger: false,
    logSeverity: 'info',
    instrumentForNewrelic: false,
};

export default defaultConfig;
