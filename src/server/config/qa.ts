import { Config, Stage } from './types';

const qaConfig: Partial<Config> = {
    stage: Stage.QA,
    instrumentForNewrelic: true,
};

export default qaConfig;
