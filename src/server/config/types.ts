export enum Stage {
    Development = 'development',
    Test = 'test',
    Dev = 'dev',
    QA = 'qa',
    Staging = 'staging',
    Production = 'production',
    Unknown = 'unknown',
}

export interface Config {
    useConsoleLogger?: boolean;
    logSeverity: 'debug' | 'info' | 'warn' | 'error';
    instrumentForNewrelic?: boolean;
    stage?: Stage;
}
