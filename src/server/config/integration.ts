import stagingConfig from './staging';
import { Config } from './types';

const integrationConfig: Partial<Config> = {
    ...stagingConfig,
    instrumentForNewrelic: false,
    logSeverity: 'error',
};

export default integrationConfig;
