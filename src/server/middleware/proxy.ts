import { Context } from 'koa';
import axios, { Method } from 'axios';
import { Agent } from 'https';

const httpsAgent = new Agent({
    rejectUnauthorized: false,
});

const proxy = (pattern: RegExp) => async (ctx: Context, next: () => void) => {
    if (!pattern.test(ctx.request.url)) return next();

    const response = await axios.request({
        httpsAgent,
        method: ctx.request.method as Method,
        // headers: ctx.request.headers,
        url: ctx.request.url,
        data: ctx.request.body,
        params: ctx.request.query,
        // baseURL: 'http://localhost:8888/',
        baseURL: 'https://airplane-problem-backend.herokuapp.com',
    });

    ctx.body = response.data;
    ctx.status = response.status;
    return (() => {})();
};

export default proxy;
