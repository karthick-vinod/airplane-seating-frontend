import './global';

import * as Koa from 'koa';

import * as cors from '@koa/cors';
import * as koa404Handler from 'koa-404-handler';
import * as errorHandler from 'koa-better-error-handler';
import * as bodyparser from 'koa-bodyparser';
import * as cacheControl from 'koa-cache-control';
import * as fallback from 'koa-connect-history-api-fallback';
import * as mount from 'koa-mount';
import * as serve from 'koa-static';

import { resolve } from 'path';

import config from './config';

import status from './controller/status';
import proxy from './middleware/proxy';

console.log(`Config loaded for stage: ${config.stage}`);

const assetsDir = resolve(__dirname, 'assets');

const app = new Koa();

/**
 * Configure and mount /api
 */

const api = new Koa();
// eslint-disable-next-line @typescript-eslint/unbound-method
api.context.onerror = errorHandler;
app.context.api = true;

api.use(cacheControl({ noCache: true }));
api.use(cors());
api.use(bodyparser());
api.use(koa404Handler);
api.use(proxy(/\/.*\//));
app.use(mount('/api', api));

/**
 * Configure and mount static asset server
 */

app.use(status.routes());
app.use(cors());

app.use(
    fallback({ htmlAcceptHeaders: ['text/html', 'application/xhtml+xml'] })
);

app.use(
    serve(assetsDir, {
        immutable: true,
        maxage: 3153600000 /* 1 year */,

        // any assets we need to exclude from forever caching, handle here.
        setHeaders: (res, path) => {
            if (path === resolve(assetsDir, 'index.html')) {
                res.setHeader('Cache-Control', 'max-age=0');
            }
        },
    })
);

export default app;
