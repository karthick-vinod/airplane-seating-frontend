/* eslint-disable global-require */
import { ServerResponse } from 'http';
import { resolve } from 'path';
import testConfig from '../config/test';

let mockCustomTransactionName: (method: string, path: string[]) => string;

let mockServeOptions: {
    immutable: boolean;
    maxage: number;
    setHeaders: (res: ServerResponse, path: string) => void;
};

jest.mock('newrelic', () => 'newRelic');

jest.mock('koa-newrelic', () =>
    jest.fn((newRelic, options) => {
        mockCustomTransactionName = options.customTransactionName;
        return () => ({});
    })
);

jest.mock('koa-static', () =>
    jest.fn((dir, options) => {
        mockServeOptions = options;
    })
);

let mockKoas: MockKoa[] = [];

class MockKoa {
    public use = jest.fn();

    public listen = jest.fn((_, cb) => cb());

    public context = {};

    public constructor() {
        mockKoas.push(this);
    }
}

jest.mock('koa', () => MockKoa);

beforeEach(() => {
    jest.clearAllMocks();
    mockKoas = [];
});

test('should use koaNewrelic if configured', () => {
    jest.resetModules();

    jest.mock('../config', () => ({
        default: { ...testConfig, instrumentForNewrelic: true },
    }));

    /* eslint-disable @typescript-eslint/no-var-requires */
    const configureKoaNewrelic = require('koa-newrelic');
    /* eslint-enable @typescript-eslint/no-var-requires */

    require('../app');

    expect(configureKoaNewrelic).toBeCalled();
    expect(mockCustomTransactionName('method', ['a', 'b'])).toBe('b (method)');
});

test('should not use koaNewrelic if not configured', () => {
    jest.resetModules();

    jest.mock('../config', () => ({
        default: { ...testConfig, instrumentForNewrelic: false },
    }));

    /* eslint-disable @typescript-eslint/no-var-requires */
    const configureKoaNewrelic = require('koa-newrelic');
    /* eslint-enable @typescript-eslint/no-var-requires */

    require('../app');

    expect(configureKoaNewrelic).not.toBeCalled();
});

test('should serve files with correct cache rules', () => {
    jest.resetModules();
    require('../app');

    expect(mockServeOptions).toMatchObject({
        immutable: true,
        maxage: 3153600000,
    });
});

test('should exempt index.html from cache rules', () => {
    jest.resetModules();
    require('../app');

    const mockRes = ({ setHeader: jest.fn() } as unknown) as ServerResponse;
    const path = resolve(__dirname, '../assets', 'index.html');

    mockServeOptions.setHeaders(mockRes, path);
    expect(mockRes.setHeader).toBeCalledWith('Cache-Control', 'max-age=0');
});

test('should ONLY exempt index.html from cache rules', () => {
    jest.resetModules();
    require('../app');

    const mockRes = ({ setHeader: jest.fn() } as unknown) as ServerResponse;
    const path = resolve(__dirname, '../assets', 'bar.js');

    mockServeOptions.setHeaders(mockRes, path);
    expect(mockRes.setHeader).not.toBeCalled();
});
