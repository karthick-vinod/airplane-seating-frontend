/* eslint-disable global-require */
let mockPort: string;
let mockListen: () => void;

jest.mock('../app', () => ({
    default: {
        listen: jest.fn((port, cb) => {
            mockListen = cb;
            mockPort = port;
        }),
    },
}));

beforeEach(jest.clearAllMocks);

test('should start on port 4000 by default', () => {
    jest.resetModules();
    require('..');

    mockListen();

    expect(mockPort).toBe('4000');
});

test('should start on provided PORT if passed', () => {
    jest.resetModules();
    process.env.PORT = '5000';
    require('..');

    mockListen();

    expect(mockPort).toBe('5000');
});
