import app from './app';

/**
 * Start server
 */

const PORT = process.env.PORT || '4000';

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});

export default app;
